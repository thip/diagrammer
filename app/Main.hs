module Main where

import Lib

main :: IO ()
main = do
    contents <- getContents
    putStrLn $ processGraph contents
