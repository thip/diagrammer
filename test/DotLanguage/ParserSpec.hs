{-# LANGUAGE OverloadedStrings #-} 

module DotLanguage.ParserSpec (spec) where

import Test.Hspec
import DotLanguage.Parser
import DotLanguage.Graph

spec :: Spec
spec = do
    describe "DotLanguage.Parser" $ do
        it "Correctly parses a graph with a single node" $ do
            parse "graph {\n  a\n}" `shouldBe` Graph [Node "a" []]
            parse "graph {\n  b\n}" `shouldBe` Graph [Node "b" []]
            parse "graph {\n  c\n}" `shouldBe` Graph [Node "c" []]
        
        it "Correctly parses a graph with multiple nodes" $ do
            parse "graph {\n  a\n  b\n}" `shouldBe` Graph [Node "a" [], Node "b" []]
            parse "graph {\n  z\n  y\n}" `shouldBe` Graph [Node "z" [], Node "y" []]
            parse "graph {\n  c\n  d\n  e\n}" `shouldBe` Graph [Node "c" [], Node "d" [], Node "e" []]

        it "Handles nodes with quoted Ids" $ do
            parse "graph {\n  \"a nice node\"\n}" `shouldBe` Graph [Node "a nice node" []]

        it "Parses node attributes when they are present" $ do
            parse "graph {\n  a [b = \"c\"]\n}" `shouldBe` Graph [Node "a" [NodeAttribute "b" "c"]]
            parse "graph {\n  aa [b = \"c\"]\n}" `shouldBe` Graph [Node "aa" [NodeAttribute "b" "c"]]
            parse "graph {\n  a [b = \"c\", d = \"e\"]\n}" `shouldBe` Graph [Node "a" [NodeAttribute "b" "c", NodeAttribute "d" "e"]]

        it "Parses graphs with edges" $ do
            parse "graph {\n  a->b\n}" `shouldBe` Graph [Edge "a" "b"]
            parse "graph {\n  a->b\n  c->d\n}" `shouldBe` Graph [Edge "a" "b", Edge "c" "d"]
            parse "graph {\n  a->b\n  c->d\n  e->f\n}" `shouldBe` Graph [Edge "a" "b", Edge "c" "d", Edge "e" "f"]
            parse "graph {\n  \"a\"->b\n}" `shouldBe` Graph [Edge "a" "b"]
            parse "graph {\n  \"a\"->\"b\"\n}" `shouldBe` Graph [Edge "a" "b"]
            parse "graph {\n  a -> b\n}" `shouldBe` Graph [Edge "a" "b"]

        it "Parses attributes when they are present" $ do
            parse "graph {\n  a=b\n}" `shouldBe` Graph [Attribute "a" "b"]
            parse "graph {\n  xx=yy\n}" `shouldBe` Graph [Attribute "xx" "yy"]
            parse "graph {\n  something=else\n}" `shouldBe` Graph [Attribute "something" "else"]
            parse "graph {\n  \"quoted\"=\"fine\"\n}" `shouldBe` Graph [Attribute "quoted" "fine"]
            parse "graph {\n  spaced = out\n}" `shouldBe` Graph [Attribute "spaced" "out"]
            parse "graph {\n  a=b\n  nodeWith [some=\"attribute\"]\n}" `shouldBe` Graph [Attribute "a" "b", Node "nodeWith" [NodeAttribute "some" "attribute"]]

        it "Parses subGraphs when they are present" $ do
            parse "graph {\n  subgraph sub {\n  }\n}" `shouldBe` Graph [SubGraph "sub" []]
            parse "graph {\n  subgraph bus {\n  }\n}" `shouldBe` Graph [SubGraph "bus" []]
            parse "graph {\n  subgraph sub {\n  a\n}\n}" `shouldBe` Graph [SubGraph "sub" [Node "a" []]]





