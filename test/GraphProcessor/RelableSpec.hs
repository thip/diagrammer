module GraphProcessor.RelableSpec (spec) where

import Test.Hspec
import DotLanguage.Graph
import GraphProcessor.Relable

spec :: Spec
spec = do
    describe "GraphProcessor.Relable" $ do
        it "modifies the label of a node" $ do
            relable (Graph [Node "a" [NodeAttribute "label" "foo"]]) "a" "bar" `shouldBe` Graph [Node "a" [NodeAttribute "label" "bar"]]
            relable (Graph [Node "b" [NodeAttribute "label" "foo"]]) "b" "bar" `shouldBe` Graph [Node "b" [NodeAttribute "label" "bar"]]
            relable (Graph [Node "b" [NodeAttribute "label" "foo"], Node "a" []]) "b" "bar" `shouldBe` Graph [Node "b" [NodeAttribute "label" "bar"], Node "a" []]
            relable (Graph [Node "b" [NodeAttribute "label" "foo"], Edge "a" "b"]) "b" "bar" `shouldBe` Graph [Node "b" [NodeAttribute "label" "bar"], Edge "a" "b"]

        it "does nothing to attributes" $ do
            relable (Graph [Attribute "foo" "foo", Node "a" [NodeAttribute "label" "foo"]]) "a" "bar" `shouldBe` Graph [Attribute "foo" "foo", Node "a" [NodeAttribute "label" "bar"]]

        it "relables nodes within subgraphs" $ do
            relable (Graph [SubGraph "sub" [Node "a" [NodeAttribute "label" "foo"]]]) "a" "bar" `shouldBe` Graph [SubGraph "sub" [Node "a" [NodeAttribute "label" "bar"]]]
