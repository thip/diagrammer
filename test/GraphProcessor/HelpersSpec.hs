module GraphProcessor.HelpersSpec (spec) where

import Test.Hspec

import DotLanguage.Graph
import GraphProcessor.Helpers

spec :: Spec
spec = do
    describe "GraphProcessor.Helpers.nodeNames" $ do
        it "returns no node names for an empty graph" $ do
            nodeNames (Graph []) `shouldBe` []
        it "returns node names for all nodes in a graph" $ do
            nodeNames (Graph [Node "a" []]) `shouldBe` ["a"]
            nodeNames (Graph [Node "b" [], Node "c" []]) `shouldBe` ["b", "c"]
            nodeNames (Graph [Edge "a" "b"]) `shouldBe` ["a", "b"]
            nodeNames (Graph [SubGraph "foo" [Node "a" []]]) `shouldBe` ["a"]
            nodeNames (Graph [Node "a" [], Edge "a" "b"]) `shouldBe` ["a", "b"]
            