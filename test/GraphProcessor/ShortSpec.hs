module GraphProcessor.ShortSpec (spec) where

import Test.Hspec

import DotLanguage.Graph

import GraphProcessor.Short

spec :: Spec
spec = do
    describe "GraphProcessor.Short" $ do
        it "Shorts nothing given given a target that does not exist" $ do
            short "q" (Graph [Edge "a" "b", Edge "b" "c"]) `shouldBe` Graph [Edge "a" "b", Edge "b" "c"]
            short "z" (Graph [Edge "c" "d", Edge "d" "e"]) `shouldBe` Graph [Edge "c" "d", Edge "d" "e"]

        it "Shorts circuits edges around matching nodes" $ do
            short "b" (Graph [Edge "a" "b", Edge "b" "c"]) `shouldBe` Graph [Edge "a" "b", Edge "b" "c", Edge "a" "c"]
            short "c" (Graph [Edge "b" "c", Edge "c" "d"]) `shouldBe` Graph [Edge "b" "c", Edge "c" "d", Edge "b" "d"]
            short "2" (Graph [Node "1" [], Node "2" [], Node "3" [], Edge "1" "2", Edge "2" "3"]) `shouldBe` Graph [Node "1" [],Node "2" [],Node "3" [], Edge "1" "2", Edge "2" "3", Edge "1" "3"]

        it "Shorts edges in subgraphs" $ do
            short "b" (Graph [SubGraph "sub" [Edge "a" "b", Edge "b" "c"]]) `shouldBe` Graph [SubGraph "sub" [Edge "a" "b", Edge "b" "c"], Edge "a" "c"]

