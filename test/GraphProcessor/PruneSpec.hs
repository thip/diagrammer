module GraphProcessor.PruneSpec (spec) where

import Test.Hspec

import DotLanguage.Graph
import GraphProcessor.Prune

spec :: Spec
spec = do
    describe "GraphProcessor.Prune" $ do
        it "Prunes nothing given an empty target list" $ do
            prune [] (Graph [Node "a" []]) `shouldBe` Graph [Node "a" []]
            prune [] (Graph [Node "b" []]) `shouldBe` Graph [Node "b" []]

        it "Prunes nodes that match a target" $ do
            prune ["b"] (Graph [Node "a" [], Node "b" []]) `shouldBe` Graph [Node "a" []]
            prune ["a"] (Graph [Node "a" [], Node "b" []]) `shouldBe` Graph [Node "b" []]
            prune [".*a.*"] (Graph [Node "bbbbabbb" [], Node "bbbbb" []]) `shouldBe` Graph [Node "bbbbb" []]

        it "Prunes edges that match a target" $ do
            prune ["b"] (Graph [Edge "a" "b", Edge "b" "c", Edge "a" "c"]) `shouldBe` Graph [Edge "a" "c"]
            prune [".*b"] (Graph [Edge "ab" "ba", Edge "ba" "ca", Edge "ab" "ca"]) `shouldBe` Graph [Edge "ba" "ca"]

        it "Ignores attributes" $ do
            prune ["a"] (Graph [Node "a" [], Attribute "a" "b"]) `shouldBe` Graph [Attribute "a" "b"]

        it "Prunes nodes within subgraphs" $ do
            prune ["b"] (Graph [SubGraph "sub" [Node "a" [], Node "b" []]]) `shouldBe` Graph [SubGraph "sub" [Node "a" []]]
