{-# LANGUAGE OverloadedStrings #-} 

module DotLanguage.Parser (parse, parseNode) where

import DotLanguage.Graph
import qualified Data.Text as T
import Text.Regex.TDFA

parse :: String -> Graph
parse theInput = Graph ((parseStatementsFrom.theInterestingBitsOf.theLinesThatMakeUp) (T.pack theInput))

theInterestingBitsOf :: [T.Text] -> [T.Text]
theInterestingBitsOf = tail.init

theLinesThatMakeUp :: T.Text -> [T.Text]
theLinesThatMakeUp text = map (T.strip) (T.lines text)

parseStatementsFrom :: [T.Text] -> [Statement]
parseStatementsFrom [] = []
parseStatementsFrom (statement:statements)
    | "}" == statement = []
    | "subgraph" `T.isPrefixOf` statement = parseSubGraph statement (parseStatementsFrom statements) : [] -- DOES NOT PARSE ANYTHING AFTERWARDS!
    | ("=" `T.isInfixOf` statement) && (not ("[" `T.isInfixOf` statement)) = parseAttribute statement : parseStatementsFrom statements
    | "->" `T.isInfixOf` statement = parseEdge statement : parseStatementsFrom statements
    | otherwise = parseNode statement : parseStatementsFrom statements

parseSubGraph :: T.Text -> [Statement] -> Statement
parseSubGraph statement statements = SubGraph nodeName statements
    where nodeName = (T.unpack . head . tail) (T.splitOn " " statement) 

parseEdge :: T.Text -> Statement
parseEdge edgeString = Edge (nodes!!0) (nodes!!1) 
    where nodes = map (T.unpack . T.strip . filterQuotesFrom) (T.splitOn "->" edgeString)

parseNode :: T.Text -> Statement
parseNode nodeString = (Node . T.unpack . filterQuotesFrom) nodeId (parseNodeAttributes attributes)
    where 
        (nodeId, _ , _, attributes)  = (nodeString =~ (" \\[(.*)\\]" :: T.Text)) :: (T.Text, T.Text, T.Text, [T.Text])

parseNodeAttributes :: [T.Text] -> [NodeAttribute]
parseNodeAttributes attributes 
    | attributes == [""] = []
    | attributes == [] = [] 
    | otherwise = map parseNodeAttribute (T.splitOn "," (head attributes))

parseAttribute :: T.Text -> Statement
parseAttribute attributeString = Attribute (T.unpack (T.strip (filterQuotesFrom left))) (T.unpack (T.strip (filterQuotesFrom (T.drop 1 right))))
    where 
        (left, right) = T.breakOn "=" attributeString

parseNodeAttribute :: T.Text -> NodeAttribute
parseNodeAttribute attribute = NodeAttribute (T.unpack (T.strip left)) (T.unpack (T.strip (filterQuotesFrom (T.drop 1 right))))
    where 
        (left, right) = T.breakOn "=" attribute


filterQuotesFrom :: T.Text -> T.Text
filterQuotesFrom = T.filter (/='"')

