module DotLanguage.Graph (
    NodeId,
    Graph (..), 
    Statement (..),
    NodeAttribute (..)
) where

type NodeId = String
type GraphId = String

data Graph = Graph [Statement] deriving (Show, Eq)

data NodeAttribute = NodeAttribute String String deriving (Show, Eq)

data Statement =  Node NodeId [NodeAttribute]
                | Edge NodeId NodeId
                | Attribute String String
                | SubGraph GraphId [Statement]  deriving (Show, Eq)
