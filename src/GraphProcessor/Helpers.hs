module GraphProcessor.Helpers (nodeNames) where

import DotLanguage.Graph
import Data.List

nodeNames :: Graph -> [String]
nodeNames (Graph statements) = nub $ something statements

something :: [Statement] -> [String]
something [] = []
something ((Node name _):statments) = (name : something  statments)
something ((Edge parent child):statments) = (parent : child : something  statments)
something ((SubGraph _ subGraphStatements):statements) = (something subGraphStatements) ++ something statements
something (_:statements) = something statements

