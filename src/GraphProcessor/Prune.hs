module GraphProcessor.Prune (prune) where

import DotLanguage.Graph
import Text.Regex.TDFA

prune :: [String] -> Graph -> Graph
prune targets (Graph (statements)) = Graph (pruneStatements targets statements)


pruneStatements :: [String] -> [Statement] -> [Statement]
pruneStatements _ [] = []

pruneStatements targets ((Node id attributes):statements)
    | matchesSomeTarget id targets = pruneStatements targets statements
    | otherwise = (Node id attributes):(pruneStatements targets statements)

pruneStatements targets ((Edge parent child):statements)
    | (matchesSomeTarget parent targets) || (matchesSomeTarget child targets) = pruneStatements targets statements
    | otherwise = (Edge parent child):(pruneStatements targets statements)

pruneStatements targets ((Attribute key value):statements) 
    = (Attribute key value):(pruneStatements targets statements)

pruneStatements targets ((SubGraph id children):statements)
    = (SubGraph id (pruneStatements targets children)):(pruneStatements targets statements)


matchesSomeTarget :: String -> [String] -> Bool
matchesSomeTarget string targets = any (\target -> (string =~ ("\\`" ++ target ++ "\\'")) :: Bool) targets

