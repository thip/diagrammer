module GraphProcessor.Short (short) where

import DotLanguage.Graph

short :: String -> Graph -> Graph
short target (Graph statements) = Graph (statements ++ something target statements [] [])

something :: String -> [Statement] -> [String] -> [String] -> [Statement]
something _ [] parents children = [Edge parent child | parent <- parents, child <- children]
something target ((Edge parent child):statements) parents children 
    | parent == target = something target statements parents (child:children)
    | child == target = something target statements (parent:parents) children
    | otherwise = something target statements parents children
something target ((SubGraph _ subGraphStatements):statements) parents children = something target (subGraphStatements ++ statements) parents children
something target (_:statements) parents children = something target statements parents children