module GraphProcessor.Relable (relable) where

import DotLanguage.Graph

relable :: Graph -> NodeId -> String -> Graph
relable (Graph statements) nodeId label = Graph (map (relableNode nodeId label) statements)

relableNode :: NodeId -> String -> Statement -> Statement
relableNode targetId label (Node nodeId attributes)
    | nodeId == targetId = Node nodeId (replaceLabel attributes label)
    | otherwise = Node nodeId attributes
relableNode targetId label (SubGraph name statements) = SubGraph name (map (relableNode targetId label) statements)
relableNode _ _ statement = statement 


replaceLabel :: [NodeAttribute] -> String -> [NodeAttribute]
replaceLabel attributes label = map (\a->arg a) attributes
    where 
        arg (NodeAttribute "label" _) =  NodeAttribute "label" label
        arg attribute = attribute