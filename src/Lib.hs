module Lib
    (
        processGraph
    ) where

import DotLanguage.Parser
import DotLanguage.Graph
import GraphProcessor.Prune
import GraphProcessor.Short
import GraphProcessor.Helpers
import Data.List

processGraph :: String -> String
processGraph input = renderGraph $ snip ".aws_acm_certificate." $ snip ".aws_route53_record." $ snip ".aws_alb_target_group." $ snip ".template_file." $ snip ".output." $ snip ".var." deOutputtedGraph
    where
        deOutputtedGraph = shortAll variableNodes prunedGraph
        variableNodes = filter (isInfixOf ".var.") nodes
        nodes = nodeNames prunedGraph
        prunedGraph = prune targets graph
        graph = parse input
        targets = [
                ".*module.label.*",
                ".*module.corsham_bastion.*",
                -- ".*module.prometheus.*", 
                ".* output\\..*",
                -- ".*.output\\..*", 
                ".* var\\..*",
                -- ".*\\.var\\..*", 
                ".* provider\\..*",
                ".* meta\\..*",
                ".*aws_vpc.*",
                -- ".*\\.aws_iam_role.*", 
                -- ".*\\.aws_iam_policy.*", 
                -- ".*\\.template_file.*", 
                ".*\\.aws_cloudwatch_log_group\\..*"
            ]

renderGraph :: Graph -> [Char]
renderGraph (Graph statements) = "digraph {\nsplines=ortho\nrankdir=LR\n" ++ intercalate "\n" (map renderStatement statements) ++ "\n}\n"

renderStatement :: Statement -> [Char]
renderStatement (Edge parent child) = "\"" ++ parent ++ "\"->\"" ++ child ++ "\""
renderStatement (Node id attributes) = "\"" ++ id ++ "\" [" ++ intercalate "," (map renderNodeAttribute attributes) ++ "]"
renderStatement (Attribute key value) = key ++ "=\"" ++ value ++ "\""
renderStatement (SubGraph id statements) = "subgraph " ++ id ++ "{\n " ++ intercalate "\n" (map renderStatement statements) ++ "\n}\n"

renderNodeAttribute :: NodeAttribute -> [Char]
renderNodeAttribute (NodeAttribute key value) = key ++ "=\"" ++ value ++ "\""


snip :: String -> Graph -> Graph
snip target graph = prune [".*" ++ target ++ ".*"] $ shortAll nodes graph where
    nodes = filter (isInfixOf target) (nodeNames graph)

shortAll :: [String] -> Graph -> Graph
shortAll [] graph = graph
shortAll (target:remainingTargets) graph = shortAll remainingTargets (short target graph)






